
function Nav() {
  return (
    <header>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <a className="navbar-brand" href="#">Conference GO!</a>
          <a className="navbar-brand" href="#">Home</a>
          <a className="navbar-brand" href="#">New location</a>
          <a className="navbar-brand" href="#">New conference</a>
          <a className="navbar-brand" href="#">New presentation</a>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
          </div>
        </div>
      </nav>
    </header>

  );
}

  export default Nav;
